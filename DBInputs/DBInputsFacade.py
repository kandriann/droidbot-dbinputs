from os.path import exists

from utilities.experiment import metrics_counter
from DBInputs.DBIExceptions import SqlException
from DBInputs.database.DatabaseStructureFactory import DatabaseStructureFactory
from DBInputs.query.CachedMicrosoftSqlQueryEngine import CachedMicrosoftSqlQueryEngine
from DBInputs.query.CachedSQLiteQueryEngine import CachedSQLiteQueryEngine
from DBInputs.query.CachedMySqlQueryEngine import CachedMySqlQueryEngine


from DBInputs.query.SqlDatabaseInfo import SqlDatabaseInfo
from DBInputs.relations.relationsmaker.DefaultRelationsMaker import DefaultRelationsMaker
from DBInputs.structure.Field import Field
from DBInputs.structure.Label import Label
from DBInputs.structure.Activity import Activity
import configparser
from random import randrange
from input_event import SetTextEvent
import random
import string


already_visited_textviews = {}  # key: activity + resource_id, value: original placeholder label
dbinputs_cache = {}  # key: activitystructure.toString(), value: DBInputs results (including input data)


class DBInputsFacade:
    # this is saved globally in order not to connect to DB any time DBInputs is called
    connected_to_DB = False
    database_struct = None

    def __init__(self, events, current_state, config):
        # the connection to DB to build DB structure and collect data is performed only once
        if not DBInputsFacade.connected_to_DB:
            parser = configparser.ConfigParser()
            parser.read(config)
            server = parser.get("config", "db_server")  # not needed by sqlite
            username = parser.get("config", "db_username")  # not needed by sqlite
            password = parser.get("config", "db_password")  # not needed by sqlite
            driver = parser.get("config", "db_driver")
            use_dbsplit = parser.get("config", "use_dbsplit") == "true" or \
                    parser.get("config", "use_dbsplit") == "True"
            if use_dbsplit:  # if dbsplit is used, then use the database half for testing
                database = parser.get("config", "test_db")
            else:  # otherwise use the full database
                database = parser.get("config", "full_db")
            sql_database_info = SqlDatabaseInfo(server, database, username, password, driver)
            if driver == "sqlserver":
                cached_sql_query_engine = CachedMicrosoftSqlQueryEngine(sql_database_info)
            elif driver == "sqlite":
                cached_sql_query_engine = CachedSQLiteQueryEngine(sql_database_info)
            elif driver == "mysql":
                cached_sql_query_engine = CachedMySqlQueryEngine(sql_database_info)
            else:
                raise SqlException("DBMS not supported (SQLite and SQL server are supported only).")
            database_struct_fact = DatabaseStructureFactory(cached_sql_query_engine)
            DBInputsFacade.database_struct = database_struct_fact.GetDatabaseStructure()
            DBInputsFacade.connected_to_DB = True
        self.views = dict()
        self.events = list()
        for event in events:
            self.views[event.view['temp_id']] = event.view
        self.activity_structure = None
        self.views_resource = list()
        self.views_key_value = dict()  # key: resource_id, value: input data

        self.CreateActivityStructure(current_state)
        self.CallDBInputs()

    def CreateActivityStructure(self, current_state):
        fields_list = list()
        package_name = self.ExtrapolateId(list(self.views.values())[0]["package"])
        activity_name = self.ExtrapolateId(current_state.foreground_activity)
        for key in self.views:
            labels_list = list()
            resource_id = self.ExtrapolateId(self.views[key]["resource_id"])
            # use resource_id as label (needed even if empty)
            labels_list.append(self.CreateLabel(resource_id))
            # get placeholder
            placeholder = self.views[key]["text"]
            # if the field has not been visited before
            if (activity_name + resource_id) not in already_visited_textviews:
                # if it's placeholder is non empty and non-numeric, save it as closest text
                if placeholder and not placeholder.isdigit():
                    already_visited_textviews[activity_name + resource_id] = placeholder
                # otherwise use the associated text computed in device_state.get_possible_inputs() as closest_text
                elif "associate_text_view" in self.views[key]:
                    already_visited_textviews[activity_name + resource_id] = (self.views[key]["associate_text_view"])[0]["text"]
                # otherwise ignore the closest text
                else:
                    already_visited_textviews[activity_name + resource_id] = None
            closest_text = already_visited_textviews[activity_name + resource_id]
            if closest_text is not None:
                labels_list.append(self.CreateLabel(closest_text))
            fields_list.append(self.CreateField(labels_list))
        self.activity_structure = self.CreateActivityStructureFromFields(fields_list, package_name, activity_name)



    def CallDBInputs(self):
        file_path = metrics_counter.set_dictionary.output_path + "\\best_matches.txt"
        file_exists = exists(file_path)
        if not file_exists:
            f = open(file_path, "x")
        else:
            f = open(file_path, "a")
        if self.activity_structure.ToString() in dbinputs_cache:
            relations_results = dbinputs_cache[self.activity_structure.ToString()]
        else:
            relations_maker = DefaultRelationsMaker("Droidbot")
            relations_results = relations_maker.FindBestMatch(self.activity_structure, DBInputsFacade.database_struct)
            dbinputs_cache[self.activity_structure.ToString()] = relations_results
            for key in relations_results.field_column_relations:  # best matches are printed
                field = relations_results.field_column_relations[key].field.ToString()
                column = relations_results.field_column_relations[key].column.ToString()
                value = str(relations_results.field_column_relations[key].value)
                type = relations_results.field_column_relations[key].similarity_type
                f.write(field + " " + column + " Similarity Value: " + value + " " + " Similarity Type: " +type+"\n")
            f.write("=========================================\n")
        f.close()
        self.ChooseInputDataForViews(relations_results)

    def CreateLabel(self, view_id):
        return Label(view_id)

    def CreateField(self, labels):
        return Field(labels)

    def CreateActivityStructureFromFields(self, fields, package, activity):
        return Activity(fields, package, activity)

    def ChooseInputDataForViews(self, relations_results):
        num_dict = dict()  # dictionary to save, for each table, the row number used to select data from
        for r in relations_results.GetRelations():
            if r.column.table_name not in num_dict:
                if r.column.Count() == 1:  # r.column.Count() return the # of record values associated with a DB column
                    num_dict[r.column.table_name] = 0
                else:
                    num_dict[r.column.table_name] = randrange(0, r.column.Count())
        for r in relations_results.GetRelations():
            resource_id = r.field.labels[0].value  # label list of each field must include resource_id even if empty
            print("Relation\n")
            print(r.field.ToString())
            print(r.column.ToString())
            text = relations_results.GetData(r.field, num_dict[r.column.table_name])
            if text == " ":
                text = "Value Null in the column " + str(r.column)
            self.views_key_value[resource_id] = text

    def ExtrapolateId(self, string):
        if string is None:
            return ""
        if "/" in string:
            resource_id = string.rsplit("/")[-1]
            if "." in resource_id:
                resource_id = resource_id.rsplit(".")[-1]
        elif "." in string:
            resource_id = string.rsplit(".")[-1]
        else:
            resource_id = string
        return resource_id

    def GetSetTextEvents(self):
        for view in self.views:
            view_resource_id = self.ExtrapolateId(self.views[view]["resource_id"])
            view_temp_id = view
            text = self.views_key_value[view_resource_id]
            if text is None or text is '':
                text = ''.join(random.choices(string.ascii_letters + string.digits, k=32))
            self.events.append(SetTextEvent(view=self.views[view_temp_id], text=text))
        return self.events
