from DBInputs.DBIExceptions import SqlException
from DBInputs.query.IQueryEngine import IQueryEngine
import logging
import sqlite3


class CachedSQLiteQueryEngine(IQueryEngine):
    MAX_NUMBER_OF_ROWS = 10000

    def __init__(self, sql_database_info):
        self.table_names = list()
        self.table_column_names = dict()
        self.table_rows = dict()
        self.logger = logging.getLoggerClass()
        self.Load(sql_database_info)

    def CreateSqlConnection(self, sql_database_info):
        connection = sqlite3.connect(sql_database_info.__getattribute__("database"))
        return connection

    def Load(self, sql_database_info):
        self.logger = logging.getLogger("Sql Database")
        self.logger.setLevel("DEBUG")
        try:
            self.logger.info("Connecting to SQLite database")
            connection = self.CreateSqlConnection(sql_database_info)
            self.logger.info("Loading data from database")
            self.LoadTableNames(connection)
            for table_name in self.table_names:
                self.LoadTableColumnNames(connection, table_name)
                self.LoadTableRows(connection, table_name)
            self.RemoveEmptyTables()
            self.logger.info("Data has been loaded")
        except:
            raise SqlException("Error in connecting to database.")

    def LoadTableNames(self, connection):
        cursor = connection.cursor()
        cursor.execute("SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%';")
        #cursor.execute("SELECT name FROM sqlite_schema WHERE type ='table' AND name NOT LIKE 'sqlite_%' AND name NOT LIKE 'tags';")
        fetch_output = cursor.fetchall()
        if fetch_output is None:
            raise SqlException("Table names are Null")
        for table in fetch_output:
            self.table_names.append(table[0])

    def LoadTableColumnNames(self, connection, table_name):
        cursor = connection.execute('SELECT * FROM ' + table_name)
        columns_names = list(map(lambda x: x[0], cursor.description))
        if columns_names is None:
            raise SqlException("Column names of table" + table_name + "are Null")
        columns_output = list()
        for column_name in columns_names:
            columns_output.append(column_name)
        self.table_column_names[table_name] = columns_output

    def LoadTableRows(self, connection, table_name):
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM " + table_name)
        rows = cursor.fetchall()
        rows_output = list()
        for row in rows:
            rows_output.append(row)
        self.table_rows[table_name] = rows_output

    def RemoveEmptyTables(self):
        not_empty_tables = list()
        for table_name in self.table_names:
            if len(self.table_column_names[table_name]) > 0 and len(self.table_rows[table_name]) > 0:
                not_empty_tables.append(table_name)
            else:
                self.table_column_names.pop(table_name)
                self.table_rows.pop(table_name)
        self.table_names = not_empty_tables

    def GetRow(self, columns):
        row = list()
        for column in columns:
            row.append(column.value)
        pass

    def GetRows(self, table_name, max_number_of_rows):
        number_of_rows = min(max_number_of_rows, len(self.table_rows[table_name]))
        i = 0
        output = list()
        for row in self.table_rows[table_name]:
            output.append(self.table_rows[table_name][i])
            i = i + 1
            if i == number_of_rows:
                break
        return output

    def GetTableColumnNames(self, table_name):
        return self.table_column_names[table_name]

    def GetTableNames(self):
        return self.table_names
