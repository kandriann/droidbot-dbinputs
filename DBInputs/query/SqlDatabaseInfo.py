from typing import Any
from pathlib import Path



class SqlDatabaseInfo:

    def __init__(self, server, database, username, password, driver):
        '''
        self.driver = "{ODBC Driver 13 for SQL Server}"#"{ODBC Driver 17 for SQL Server}"
        self.server = "localhost\SQLEXPRESS01"#DESKTOP-FQM5DAJ
        self.database = "ProvaDroidbot"
        self.uid = "diego"#root
        self.pwd = "Password2"#password
        '''
        if driver == "sqlserver":
            self.driver = "{ODBC Driver 13 for SQL Server}"  # this is used only if 'sqlserver' is set as driver
        elif driver == "sqlite":
            full_db_file = Path(database)
            if not full_db_file.is_file():
                raise Exception('No actual database was found. Check the path in config file.')
        self.server = server
        self.database = database
        self.uid = username
        self.pwd = password

    def __setattr__(self, name: str, value: Any) -> None:
        super().__setattr__(name, value)

    def __getattribute__(self, name: str) -> Any:
        return super().__getattribute__(name)