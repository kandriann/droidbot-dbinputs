from typing import Any

from DBInputs.DBIExceptions import ArgumentNullException, ArgumentOutOfRangeException


class FieldColumnRelation:
    def __init__(self, field, column, value, similarity_type):
        if field is None:
            raise ArgumentNullException("Field")
        if column is None:
            raise ArgumentNullException("Column")
        if value is None or value < 0 or value > 1:
            raise ArgumentOutOfRangeException("Quality must be in the range [0;1]")
        similarity_type = similarity_type.replace('String', '')
        similarity_type = similarity_type.replace('Comparer', '')
        if similarity_type is None or similarity_type not in ('EditDistance', 'Word2Vec', 'Synonym', 'Equivalent'):
            raise ArgumentOutOfRangeException("Predominance is wrong")
        self.field = field
        self.column = column
        self.value = value
        self.similarity_type = similarity_type

    def __getattribute__(self, name: str) -> Any:
        return super().__getattribute__(name)

    def Equals(self, obj):
        return isinstance(obj, FieldColumnRelation) and self.field.Equals(obj.field) and self.column.Equals(obj.column)

    def __hash__(self) -> int:
        return super().__hash__()

    def ToString(self):
        return "Q: " + str(self.value) + " " + self.field.ToString() + " " + self.column.ToString()
