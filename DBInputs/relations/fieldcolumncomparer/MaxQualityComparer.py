from DBInputs.DBIExceptions import ArgumentNullException
from DBInputs.relations.fieldcolumncomparer.IFieldColumnComparer import IFieldColumnComparer


class MaxQualityComparer(IFieldColumnComparer):
    def __init__(self, string_comparer):
        self.string_comparer = string_comparer

    def Compare(self, field, column):
        if field is None:
            raise ArgumentNullException("Field")
        if column is None:
            raise ArgumentNullException("Column")
        maximum = 0
        # the label with highest value determine the comparer as winner for the current field-column
        similarity_type = "Equivalent"
        for label in field.labels:
            if label and label.value:  # use only non-empty labels
                compare_result = self.string_comparer.StringSimilarity(label.value, column.name)
                quality = compare_result[0]  # compare_result[0] is the similarity value
                if quality == maximum:
                    similarity_type = "Equivalent"
                if quality > maximum:
                    maximum = quality
                    similarity_type = compare_result[1]
        return maximum, similarity_type

    def GetComparer(self):
        return self.string_comparer
