from DBInputs.DBIExceptions import ArgumentNullException
from DBInputs.relations.fieldcolumncomparer.IFieldColumnComparer import IFieldColumnComparer


class MeanQualityComparer(IFieldColumnComparer):
    def __init__(self, string_comparer):
        self.string_comparer = string_comparer

    def Compare(self, field, column):
        if field is None:
            raise ArgumentNullException("Field")
        if column is None:
            raise ArgumentNullException("Column")
        quality = 0
        # the following counters are used to compute the times a similarity algorithm is predominant among labels
        # the one winning in most labels is the one considered as winner for the current field-column
        num_word2vec = 0
        num_synonym = 0
        num_editdistance = 0
        for label in field.labels:
            if label and label.value:  # use only non-empty labels
                compare_result = self.string_comparer.StringSimilarity(label.value, column.name)
                quality = quality + compare_result[0]  # compare_result[0] is the similarity value
                # compute which similarity algorithm won on this label
                if 'Word2Vec' in compare_result[1]:
                    num_word2vec = num_word2vec + 1
                elif 'Synonym' in compare_result[1]:
                    num_synonym = num_synonym + 1
                elif 'EditDistance' in compare_result[1]:
                    num_editdistance = num_editdistance + 1
        if num_word2vec > num_synonym and num_word2vec > num_editdistance:
            similarity_type = 'Word2Vec'
        elif num_synonym > num_word2vec and num_synonym > num_editdistance:
            similarity_type = "Synonym"
        elif num_editdistance > num_word2vec and num_editdistance > num_synonym:
            similarity_type = "EditDistance"
        else:
            similarity_type = "Equivalent"
        return quality / len(field.labels), similarity_type

    def GetComparer(self):
        return self.string_comparer