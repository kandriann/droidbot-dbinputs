from DBInputs.DBIExceptions import NotImplementedException
from DBInputs.relations.fieldcolumncomparer import StringTransformation
from DBInputs.relations.fieldcolumncomparer.stringcomparer import Tokenizer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.CachedLemmatizer import CachedLemmatizer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.IStringComparer import IStringComparer
import nltk.metrics.distance


class EditDistanceStringComparer(IStringComparer):
    def __init__(self):
        self.cached_words_similarity = dict()

    def StringSimilarity(self, s1, s2):
        if len(s1) == 0 or len(s2) == 0:
            return 0
        if self.cached_words_similarity.__contains__(s1 + s2):
            return self.cached_words_similarity[s1 + s2]
        if self.cached_words_similarity.__contains__(s2 + s1):
            return self.cached_words_similarity[s2 + s1]
        s1_tokens = self.GetLemmatizedTokens(s1)
        s2_tokens = self.GetLemmatizedTokens(s2)
        if len(s1_tokens) == 0 or len(s2_tokens) == 0:
            self.cached_words_similarity[s1 + s2] = self.cached_words_similarity[s2 + s1] = 0
            return 0
        '''
        from os.path import exists
        file_exists = exists("..\\lemmas.txt")
        if not file_exists:
            f = open("..\\lemmas.txt", "x")
        else:
            f = open("..\\lemmas.txt", "a")
        f.write("Original word " + s1 + "\n")
        for lemma in s1_tokens:
            f.write("EditDistance token " + lemma + "\n")
        f.write("========================\n")
        f.close()
        '''
        if len(s1_tokens) >= len(s2_tokens):
            longest = s1_tokens
            shortest = s2_tokens
        else:
            longest = s2_tokens
            shortest = s1_tokens
        sum = 0.0
        for t1 in longest:
            max_similarity = 0.0
            for t2 in shortest:
                similarity = self.GetEditDistance(t1, t2)
                if similarity > max_similarity:
                    max_similarity = similarity
            sum = sum + max_similarity
        mean = sum / len(longest)
        self.cached_words_similarity[s1 + s2] = self.cached_words_similarity[s2 + s1] = mean
        return mean

    def GetLemmatizedTokens(self, s):
        s = ''.join([i for i in s if not i.isdigit()])  # remove digits before tokenization
        tokens = Tokenizer.SplitSeparators(Tokenizer.SplitCamelCase(s))
        clean_tokens = set()
        for token in tokens:
            clean_token = StringTransformation.CleanString(token)
            if clean_tokens != "":
                clean_tokens.add(clean_token.lower())
        lemmatized_tokens = set()
        for token in clean_tokens:
            if token not in ('', 'edit', 'text', 'view', 'auto', 'complete', 'placeholder', 'src', 'your',
                             'autocomplete', 'input', 'change', 'new', 'add', 'ac', 'search',
                             'to', 'of', 'in', 'for', 'per', 'as', 'or', 'and', 'at', 'by',
                             'the', 'I', 'you', 'he', 'she', 'they', 'on', 'do', 'edt') and clean_token.isalpha():
                lemmatized_tokens.add(CachedLemmatizer.Lemmatize(CachedLemmatizer(), token))
        return lemmatized_tokens

    def GetEditDistance(self, s1, s2):
        if not (self.cached_words_similarity.__contains__(s1 + s2) or self.cached_words_similarity.__contains__(s1 + s2)):
            edit_distance = nltk.edit_distance(s1, s2)
            len_s1 = len(s1)
            len_s2 = len(s2)
            len_tot = len_s1 + len_s2
            similarity = 1 - (edit_distance/len_tot)
            if similarity < 0:
                similarity = 0
            else:
                if similarity > 1:
                    similarity = 1
            self.cached_words_similarity[s1 + s2] = similarity
            self.cached_words_similarity[s2 + s1] = similarity
        return self.cached_words_similarity[s1 + s2]

    def GetComparers(self):
        raise NotImplementedException()
