from DBInputs.relations.fieldcolumncomparer.stringcomparer.IStringComparer import IStringComparer


class MaxComposedStringComparer(IStringComparer):
    def __init__(self):
        self.string_comparers = list()

    def GetStringComparers(self):
        list2 = self.string_comparers
        return list2

    def AddComparer(self, comparer):
        self.string_comparers.append(comparer)

    def StringSimilarity(self, s1, s2):
        max = 0
        max_comparer = 'Equivalent'  # this is used to store the type of best algorithm similarity for current strings
        for comparer in self.string_comparers:
            similarity = comparer.StringSimilarity(s1, s2)
            # the highest comparer is considered. if no best one is found, they are equivalent
            if similarity == max:
                max_comparer = 'Equivalent'
            elif similarity > max:
                max_comparer = comparer.__class__.__name__
                max = similarity
        return max, max_comparer

    def GetComparers(self):
        return self.string_comparers