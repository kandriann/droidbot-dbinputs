from DBInputs.relations.fieldcolumncomparer.stringcomparer.IStringComparer import IStringComparer


class MeanComposedStringComparer(IStringComparer):
    def __init__(self):
        self.string_comparers = list()

    def GetStringComparers(self):
        new_list = self.string_comparers
        return new_list

    def AddComparer(self, comparer):
        self.string_comparers.append(comparer)

    def StringSimilarity(self, s1, s2):
        sum = 0
        max = 0
        max_comparer = 'Equivalent'  # this is used to store the type of best algorithm similarity for current strings
        for comparer in self.string_comparers:
            similarity = comparer.StringSimilarity(s1, s2)
            # although the mean of comparers is considered, the highest one is stored to
            # determine the best one. if no best one is found, they are equivalent
            if similarity == max:
                max_comparer = 'Equivalent'
            elif similarity > max:
                max_comparer = comparer.__class__.__name__
                max = similarity
            sum = sum + similarity
        return sum / len(self.string_comparers), max_comparer

    def GetComparers(self):
        return self.string_comparers