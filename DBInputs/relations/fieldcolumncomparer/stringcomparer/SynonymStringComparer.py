from nltk.corpus import wordnet
from DBInputs.DBIExceptions import NotImplementedException
from DBInputs.relations.fieldcolumncomparer.StringTransformation import CleanString
from DBInputs.relations.fieldcolumncomparer.stringcomparer.CachedLemmatizer import CachedLemmatizer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.IStringComparer import IStringComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.Tokenizer import SplitCamelCase, SplitSeparators
from DBInputs.relations.fieldcolumncomparer.stringcomparer.WordNetUtilities import WordNetUtilities


class SynonymStringComparer(IStringComparer):
    def __init__(self):
        self.SYNONYM_SIMILARITY = 0.9
        self.word_net_utilities = WordNetUtilities()
        self.cached_lemmatizer = CachedLemmatizer()
        if not self.word_net_utilities.is_loaded:
            self.word_net_utilities.Load()
        if not self.cached_lemmatizer.is_loaded:
            self.cached_lemmatizer.Load()
        self.cached_synonyms = dict()
        self.cached_words_similarity = dict()

    # OLD
    def StringSimilarity2(self, s1, s2):
        s1_lemmas = self.GetLemmas(s1)
        s2_lemmas = self.GetLemmas(s2)
        intersection = 0
        for lemma in s1_lemmas:
            if lemma in s2_lemmas:
                intersection = intersection + 1
            else:
                synonyms = self.GetSynonyms(lemma)
                intersection = intersection + self.IntersectionValue(s2_lemmas, synonyms)
        similarity = self.ComputeSimilarity(s1_lemmas, s2_lemmas, intersection)
        if similarity is None:
            self.cached_words_similarity[s1 + s2] = self.cached_words_similarity[s2 + s1] = 0
            return 0
        else:
            self.cached_words_similarity[s1 + s2] = self.cached_words_similarity[s2 + s1] = similarity
            return similarity

    def GetLemmas(self, s):
        s = ''.join([i for i in s if not i.isdigit()])  # remove digits before tokenization
        tokens = set(self.Tokenize(s))
        clean_tokens = set()
        for token in tokens:
            clean_token = CleanString(token)
            if clean_token not in ('', 'edit', 'text', 'view', 'auto', 'complete', 'placeholder', 'src', 'your',
                                   'autocomplete', 'input', 'change', 'new', 'add', 'ac', 'search',
                                   'to', 'of', 'in', 'for', 'per', 'as', 'or', 'and', 'at', 'by',
                                   'the', 'I', 'you', 'he', 'she', 'they', 'on', 'do', 'edt') and clean_token.isalpha():
                clean_tokens.add(clean_token)
        return self.Lemmatize(clean_tokens)

    def Lemmatize(self, tokens):
        lemmatized_tokens = set()
        for token in tokens:
            lemmatized_tokens.add(self.cached_lemmatizer.Lemmatize(token))
        return lemmatized_tokens

    def Tokenize(self, str):
        return SplitSeparators(SplitCamelCase(str))

    def GetComparers(self):
        raise NotImplementedException()

    def GetSynonyms(self, lemma):
        if lemma in self.cached_synonyms:
            return self.cached_synonyms[lemma]
        syn_set_list = self.word_net_utilities.GetSynSets(lemma)
        synonyms = set()
        for syn_set in syn_set_list:
            synonyms.add(syn_set.lemmas()[0].name())
        if lemma in synonyms:
            synonyms.remove(lemma)
        self.cached_synonyms[lemma] = synonyms
        return synonyms

    def IntersectionValue(self, s2_lemmas, synonyms):
        for synonym in synonyms:
            if synonym in s2_lemmas:
                return self.SYNONYM_SIMILARITY
        return 0

    def ComputeSimilarity(self, s1_lemmas, s2_lemmas, intersection):
        return intersection / (len(s1_lemmas) + len(s2_lemmas) - intersection)

    def StringSimilarity(self, s1, s2):
        if len(s1) == 0 or len(s2) == 0:
            return 0
        if self.cached_words_similarity.__contains__(s1 + s2):
            return self.cached_words_similarity[s1 + s2]
        if self.cached_words_similarity.__contains__(s2 + s1):
            return self.cached_words_similarity[s2 + s1]
        s1_lemmas = self.GetLemmas(s1)
        s2_lemmas = self.GetLemmas(s2)
        if len(s1_lemmas) == 0 or len(s2_lemmas) == 0:
            self.cached_words_similarity[s1 + s2] = self.cached_words_similarity[s2 + s1] = 0
            return 0
        '''
        from os.path import exists
        file_exists = exists("..\\lemmas.txt")
        if not file_exists:
            f = open("..\\lemmas.txt", "x")
        else:
            f = open("..\\lemmas.txt", "a")
        f.write("Original word " + s1 + "\n")
        for lemma in s1_lemmas:
            f.write("Wordnet token " + lemma + "\n")
        f.write("========================\n")
        f.close()
        '''
        tot = 0
        num = 0
        for s1_lemma in s1_lemmas:
            set_1 = wordnet.synsets(s1_lemma)
            for s2_lemma in s2_lemmas:
                set_2 = wordnet.synsets(s2_lemma)
                max_value = 0
                for word_set_1 in set_1:
                    for word_set_2 in set_2:
                        value = word_set_1.wup_similarity(word_set_2)
                        if value is not None and value > max_value:
                            max_value = value
                tot = tot + max_value
                num = num + 1
        if num == 0:
            num = 1
        tot = tot / num
        self.cached_words_similarity[s1 + s2] = self.cached_words_similarity[s2 + s1] = tot
        return tot
