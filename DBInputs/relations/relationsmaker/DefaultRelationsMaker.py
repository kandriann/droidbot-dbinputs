from DBInputs.DBIExceptions import ArgumentNullException
from py_linq import Enumerable
from DBInputs.relations.FieldColumnRelation import FieldColumnRelation
from DBInputs.relations.ActivityDatabaseRelation import ActivityDatabaseRelation
# from DBInputs.relations.clustering.MyDBSCAN import MyDBSCAN
from DBInputs.relations.clustering.MyDBSCAN import MyDBSCAN
from DBInputs.relations.fieldcolumncomparer.MaxQualityComparer import MaxQualityComparer
from DBInputs.relations.fieldcolumncomparer.MeanQualityComparer import MeanQualityComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.DamerauLevenshteinStringComparer import \
    DamerauLevenshteinStringComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.EditDistanceStringComparer import EditDistanceStringComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.MaxComposedStringComparer import MaxComposedStringComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.MeanComposedStringComparer import MeanComposedStringComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.SynonymStringComparer import SynonymStringComparer
from DBInputs.relations.fieldcolumncomparer.stringcomparer.Word2VecComparer import Word2VecComparer
from DBInputs.relations.relationsmaker.IRelationsMaker import IRelationsMaker
from DBInputs.relations.setcover.MinimumSetCoverAlgorithm import MinimumSetCoverAlgorithm


class DefaultRelationsMaker(IRelationsMaker):
    NUM_BREAKS = 13

    def __init__(self, relation_maker_type, field_column_comparer=None, set_cover_algorithm=None):
        if field_column_comparer is None and set_cover_algorithm is None:
            if relation_maker_type == "MAXSIMILARITY_MAXLABELS_OLD":
                string_comparer = MaxComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(SynonymStringComparer())
                string_comparer.AddComparer(DamerauLevenshteinStringComparer())
                self.field_column_comparer = MaxQualityComparer(string_comparer)
            if relation_maker_type == "MEANSIMILARITY_MAXLABELS_OLD":
                string_comparer = MeanComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(SynonymStringComparer())
                string_comparer.AddComparer(DamerauLevenshteinStringComparer())
                self.field_column_comparer = MaxQualityComparer(string_comparer)
            if relation_maker_type == "MAXSIMILARITY_MEANLABELS_OLD":
                string_comparer = MaxComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(SynonymStringComparer())
                string_comparer.AddComparer(DamerauLevenshteinStringComparer())
                self.field_column_comparer = MeanQualityComparer(string_comparer)
            if relation_maker_type == "MEANSIMILARITY_MEANLABELS_OLD":
                string_comparer = MeanComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(SynonymStringComparer())
                string_comparer.AddComparer(DamerauLevenshteinStringComparer())
                self.field_column_comparer = MeanQualityComparer(string_comparer)
            if relation_maker_type == "MAXSIMILARITY_MAXLABELS_NEW":
                string_comparer = MaxComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(Word2VecComparer(False))
                self.field_column_comparer = MaxQualityComparer(string_comparer)
            if relation_maker_type == "MEANSIMILARITY_MAXLABELS_NEW":
                string_comparer = MeanComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(Word2VecComparer(False))
                self.field_column_comparer = MaxQualityComparer(string_comparer)
            if relation_maker_type == "MAXSIMILARITY_MEANLABELS_NEW":
                string_comparer = MaxComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(Word2VecComparer(False))
                self.field_column_comparer = MeanQualityComparer(string_comparer)
            if relation_maker_type == "MEANSIMILARITY_MEANLABELS_NEW":
                string_comparer = MeanComposedStringComparer()
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(Word2VecComparer(False))
                self.field_column_comparer = MeanQualityComparer(string_comparer)
            # newest version implemented
            if relation_maker_type == "Droidbot":
                #string_comparer = MeanComposedStringComparer()
                string_comparer = MaxComposedStringComparer()
                #string_comparer.AddComparer(Word2VecComparer(False))
                string_comparer.AddComparer(EditDistanceStringComparer())
                string_comparer.AddComparer(SynonymStringComparer())
                #self.field_column_comparer = MaxQualityComparer(string_comparer)
                self.field_column_comparer = MeanQualityComparer(string_comparer)
            self.set_cover_algorithm = MinimumSetCoverAlgorithm()
        else:
            self.field_column_comparer = field_column_comparer
            self.set_cover_algorithm = set_cover_algorithm

    def FindBestMatch(self, activity, database):
        if activity is None:
            raise ArgumentNullException("Activity")
        if database is None:
            raise ArgumentNullException("Database")
        if len(activity.GetFields()) == 0:
            return self.CreateActivityDatabaseRelation(list(), activity)
        all_sims = self.ComputeAllSimilarities(activity, database)

        #item = [x for x in all_sims if x.column.name == 'name']

        filtered_sims = self.FilterSimilarities(activity, all_sims)
        all_matches = self.GenerateAllMatches(activity, filtered_sims)
        best_match = self.EvaluateBestMatch(activity, all_matches)
        return self.CreateActivityDatabaseRelation(best_match, activity)

    def CreateActivityDatabaseRelation(self, best_match, activity):
        best_match = Enumerable(best_match)
        field_column_relation = dict()
        for field in activity.GetFields():
            n_matches = best_match.count()
            field_column_relation[field] = best_match.where(lambda fc: fc.field.Equals(field)).first()
        return ActivityDatabaseRelation(activity, field_column_relation, MinimumSetCoverAlgorithm().current_best_value)


    def ComputeAllSimilarities(self, activity, database):
        all_sims = list()
        for field in activity.GetFields():
            for key, table in database.tables.items():
                for column in table.columns:
                    result = self.field_column_comparer.Compare(field, column)
                    sim = FieldColumnRelation(field, column, result[0], result[1])  # result[0] is similarity value
                                                                                    # result[1] is similarity type
                    all_sims.append(sim)
        return all_sims

    def FilterSimilarities(self, activity, all_sims):
        filtered_sims = Enumerable(list())
        all_sims_enum = Enumerable(all_sims)
        for field in activity.GetFields():
            sim_for_field = all_sims_enum.where(lambda s: s.field.Same(field)).to_list()
            filtered_sims = filtered_sims.concat(Enumerable(MyDBSCAN().DBSCANForSims(sim_for_field)))
        schemas = dict()
        for fc in filtered_sims:
            if fc.column.table_name not in schemas:
                columns = list()
                columns.append(fc.column.name)
                schemas[fc.column.table_name] = columns
            else:
                if fc.column.name not in schemas[fc.column.table_name]:
                    new_columns_list = schemas[fc.column.table_name]
                    new_columns_list.append(fc.column.name)
                    schemas[fc.column.table_name] = new_columns_list
        work_list = list(schemas.keys())
        for first_schema in schemas.keys():
            work_list.remove(first_schema)
            first_schema_similarity = self.ComputeActivityTableSimilarity(activity, first_schema)
            for second_schema in work_list:
                first_schema_enum = Enumerable(schemas[first_schema])
                second_schema_enum = Enumerable(schemas[second_schema])
                first_contained = first_schema_enum.all(lambda i: i in schemas[second_schema])
                second_contained = second_schema_enum.all(lambda i: i in schemas[first_schema])
                equal = first_contained and second_contained
                second_schema_similarity = self.ComputeActivityTableSimilarity(activity, second_schema)
                if (first_contained and not equal) or (equal and first_schema_similarity <= second_schema_similarity):
                    filtered_sims = [item for item in filtered_sims if item.column.table_name != first_schema]
                    break
                if (second_contained and not equal) or (equal and second_schema_similarity <= first_schema_similarity):
                    filtered_sims = [item for item in filtered_sims if item.column.table_name != second_schema]
        schemas = dict()
        for fc in filtered_sims:
            if fc.column.table_name not in schemas:
                columns = list()
                columns.append(fc.column.name)
                schemas[fc.column.table_name] = columns
            else:
                if fc.column.name not in schemas[fc.column.table_name]:
                    new_columns_list = schemas[fc.column.table_name]
                    new_columns_list.append(fc.column.name)
                    schemas[fc.column.table_name] = new_columns_list
        filtered_sims = self.unique_values(filtered_sims)
        return filtered_sims

    def unique_values(self, iterable):
        seen = set()
        for item in iterable:
            if item not in seen:
                seen.add(item)
        return seen

    def GenerateAllMatches(self, activity, filtered_sims):
        return self.set_cover_algorithm.GenerateAllMatches(activity, filtered_sims)

    def EvaluateBestMatch(self, activity, all_matches):
        return self.set_cover_algorithm.EvaluateBestMatch(activity, all_matches)

    def ComputeActivityTableSimilarity(self, activity, table):
        comparers = self.field_column_comparer.GetComparer().GetComparers()
        activity_name = self.split(activity.GetActivity())
        package_name = self.split(activity.GetPackage())
        activity_semantic_similarity = comparers[0].StringSimilarity(activity_name, table)
        activity_syntactic_similarity = comparers[1].StringSimilarity(activity_name, table)
        activity_similarity = max(activity_semantic_similarity, activity_syntactic_similarity)
        package_semantic_similarity = comparers[0].StringSimilarity(package_name, table)
        package_syntactic_similarity = comparers[1].StringSimilarity(package_name, table)
        package_similarity = max(package_semantic_similarity, package_syntactic_similarity)
        return (activity_similarity + package_similarity) / 2

    def split(self, string):
        if "/" in string:
            string = string.rsplit("/")[-1]
        if "." in string:
            string = string.rsplit(".")[-1]
        '''
        index = package.rfind('/')
        if index > 0:
            package = package[index + 1:]
        index = package.rfind('/')
        if index > 0:
            package = package[:index]
        '''
        return string
