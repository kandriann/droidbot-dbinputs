from DBInputs.DBIExceptions import NotImplementedException
from DBInputs.relations.setcover.ISetCoverAlgorithm import ISetCoverAlgorithm


class GreedySetCoverAlgorithm(ISetCoverAlgorithm):
    def __init__(self):
        pass

    def EvaluateBestmatch(self, activity, all_combinations):
        raise NotImplementedException()

    def GenerateAllMatches(self, activity, all_matches):
        raise NotImplementedException()