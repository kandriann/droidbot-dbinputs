class Activity:
    def __init__(self, fields, package=None, activity=None):
        if fields is None or None in fields:
            raise Exception("Activity: fields is None or None is in field")
        self.fields = fields
        self.package = package
        self.activity = activity

    def GetFieldCount(self):
        return len(self.fields)

    def Equals(self, obj):
        if not isinstance(obj, Activity) or obj is None:
            return False
        other_activity = obj
        if len(self.fields) != len(other_activity.fields):
            return False
        for i in range(len(self.fields)):
            if self.fields[i].Same(other_activity.fields[i]):
                pass
            else:
                return False
        return True

    def GetFields(self):
        return self.fields

    def GetActivity(self):
        return self.activity

    def GetPackage(self):
        return self.package

    def ToString(self):
        stringresult = "Activity Structure : ["
        if self.package is not None:
            stringresult = stringresult + " Package: " + self.package
        if self.activity is not None:
            stringresult = stringresult + " Activity : " + self.activity
        stringresult = stringresult + " Fields: "
        for field in self.fields:
            stringresult = stringresult + "(" + field.labels[0].value + "), "
            # stringresult = stringresult + "(" + field.ToString() + "), "
        return stringresult + "]"

    def GetHashCode(self):
        hash_code = 3042524
        for field in self.fields:
            hash_code = hash_code + field.GetHashCode()
        return hash_code
