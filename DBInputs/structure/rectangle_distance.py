# https://stackoverflow.com/questions/4978323/how-to-calculate-distance-between-two-rectangles-context-a-game-in-lua
# to compute closest views and find related text used by DBInputs
def rect_distance(r1, r2):
    xmin1 = r1[0][0]
    xmax1 = r1[1][0]
    ymin1 = r1[0][1]
    ymax1 = r1[1][1]
    xmin2 = r2[0][0]
    xmax2 = r2[1][0]
    ymin2 = r2[0][1]
    ymax2 = r2[1][1]
    left = xmax2 < xmin1
    right = xmax1 < xmin2
    bottom = ymax2 < ymin1
    top = ymax1 < ymin2
    if top and left:
        return point_distance((xmin1, ymax1), (xmax2, ymin2))
    elif left and bottom:
        return point_distance((xmin1, ymin1), (xmax2, ymax2))
    elif bottom and right:
        return point_distance((xmax1, ymin1), (xmin2, ymax2))
    elif right and top:
        return point_distance((xmax1, ymax1), (xmin2, ymin2))
    elif left:
        return xmin1 - xmax2
    elif right:
        return xmin2 - xmax1
    elif bottom:
        return ymin1 - ymax2
    elif top:
        return ymin2 - ymax1
    else:  # rectangles intersect
        return 0.


def point_distance(p1, p2):
    result = ((((p2[0] - p1[0]) ** 2) + ((p2[1] - p1[1]) ** 2)) ** 0.5)
    return result
