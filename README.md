# Droidbot + DBInputs
Droidbot+DBInputs extends the original Droidbot tool \[1\] with DBInputs \[2\], a technique for automatic GUI testing that feeds input fields with meaningful data extracted from the target application database. Applications' databases are natively crafted with application-specific data and natively designed with domain-specific concepts, and are often available during system/regression testing phases, therefore the data they store can - by construction - fulfil the complex contraints defined over input fields, which may otherwise damage the effectiveness of the test exploration. To retrieve meaningful input data, DBInputs automatically resolves the mismatch between the user interface and the schema of the database of the application under test, by computing syntactic and semantic similarities between the identifiers of the input fields shown on the GUI screen and the ones in the tables of the database. Once the most relevant columns w.r.t the current GUI are found, input data records are returned to Droidbot to feed the input fields with. 

From the original Droidbot version, the following main changes occurred: 
- ComplexEvent input event type has been added, in order to consider also events that may require the execution of multiple input events all at once (e.g., a form submission that needs all the input fields to be filled before clicking the submit button)
- DBInputs, random alphanumeric strings, and Link (i.e., a tool that queries over DBPedia) \[3\] input generators have been integrated in the tool
- Droidbot has been enabled to accept a configuration file as input parameter to instrument DBInputs 
- Loggers have been added to track several input generators performance metrics (e.g., time needed to query a database, number of submissions performed with valid or invalid data)


## Prerequisites
To run Droidbot+DBInputs, you need: 
- Java SE 15 or later 
- Android SDK 11 API Level 30 or later
- Python 3.8
- An Android emulator properly configured (e.g., Nexus 5 via Android Studio) or a physical device connected
- Additional dependencies installed
	- `pip install future`
	- `pip install --upgrade pyodbc`
	- `pip install py-linq`
	- `pip install nltk`
	- From Python shell:
	    - `import nltk`
		- `nltk.download('wordnet')`
		- `nltk.download('omw-1.4')`
- Environmental variables configured
	- path\to\java-jdk\bin
	- path\to\android-sdk\platform-tools
	- path\to\android-sdk\tools
	- path\to\android-sdk\emulator 
	- path\to\Python38
	- path\to\Python38\Scripts
- Droidbot+DBInputs downloaded and installed 
	- `git clone https://gitlab.com/kandriann/droidbot-dbinputs`
	- `cd droidbot-dbinputs\`
	- `pip install -e .`
- An APK and a database (any SQL-based DBMS should be accepted) available. The APK does not need to be installed before using Droidbot.


## How to use Droidbot+DBInputs
To run the tool: 
1. Run the Android emulator (or connect a physical device to the computer)
2. From cmd shell, launch `droidbot -a "path\to\APK" -o "path\to\OUTPUTS" -is_emulator -random -count NUM_EVENTS -db_config "path\to\DBInputs\CONFIG"`

`APK` must include ".apk" extension. `OUTPUTS` folder will contains the output files produced by Droidbot during the exploration, such as visited GUI screens, navigational model, and further log informations. `NUM_EVENTS` indicates the number of input events to execute (e.g., 100). 

`CONFIG` file is used to instrument DBInputs (an example file is found under Droidbot+DBInputs installation folder), and is described as follows: 
- **input_data_technique**: it defines the input generator used by Droidbot. It must be set to _default_ for baseline, _dbinputs_ for DBInputs, _random_ for random, or _link_ for Link;
- **use_dbsplit**: set to _true_ if a split of the database using DBSplit plug-in \[4\] is desired. The split process will produce two databases, one used to configure the app initial state and one used for testing. DBSplit performs backup at the end of the testing activity;
- **use_device_db**: set to _true_ if the database to consider is the one currently run by the application. By default, it is set to _false_ since Droidbot usually performs fresh installations; 
- **dbsplit_jar_path**: the path to DBSplit jar, needed if **use_dbsplit** is set to _true_. By default, the jar file is under the downloaded Droidbot+DBInputs folder; 
- **link_jar_path**: the path to Link jar, needed if **input_data_technique** is set to _link_. By default, the jar file is under the downloaded Droidbot+DBInputs folder; 
- **db_server**, **db_username**, **db_password**: not needed by the current implementation, which relies on SQLite DBMS; 
- **db_driver**: the DBMS driver used to connect with the database. By default, it is set to _sqlite_; 
- **full_db**, **test_db**, **app_db**: the paths to original (full) and two halves (test and app) databases, the latter produced by DBSplit. **full_db** must be coherent with the name of the actual database used by the application. If **use_dbsplit** is set to _false_, only **full_db** is needed; if **use_dbsplit** is set to _true_, **test_db** and **app_db** must not exist before running DBSplit, otherwise no split will occur and the given files will be used for testing. If **use_device_db** is set to _true_, **full_db** is the path where the database will be pulled to;
- **split_percent**: the split % between **test_db** and **app_db** databases performed by DBSplit over **full_db** database, if **use_dbsplit** is set to _true_. If the % is 50, **test_db** and **app_db** databases will likely have the same size; 
- **split_type**: leave this value set to _overlapping_; 
- **overlapping_percent**: the overlapping % between **test_db** and **app_db** databases performed by DBSplit over **full_db** database, if **use_dbsplit** is set to _true_. If the % is 50, **test_db** and **app_db** databases will likely share half the records;


## External References
\[1\]: Y. Li, Z. Yang, Y. Guo, and X. Chen, "Droidbot: A lightweight UI-guided test input generator for Android", In _2017 IEEE/ACM 39th International Conference on Software Engineering Companion (ICSE-C)_, 2017, pp. 23-26, downloadable at: https://www.researchgate.net/publication/318125264_DroidBot_A_Lightweight_UI-Guided_Test_Input_Generator_for_Android, repository: https://github.com/honeynet/droidbot.git

\[2\]: D. Clerissi, G. Denaro, M. Mobilio, and L. Mariani, "Plug the database & play with automatic testing: Improving system testing by exploiting persistent data", In _Proceedings of the 35th IEEE/ACM International Conference on Automated Software Engineering (ASE)_, 2020, pp. 66–77, downloadable at: https://www.researchgate.net/publication/344427891_Plug_the_Database_Play_With_Automatic_Testing_Improving_System_Testing_by_Exploiting_Persistent_Data, 
repository: https://gitlab.com/DBInputs/dbinputs

\[3\]: L. Mariani, M. Pezzè, O. Riganelli, and M. Santoro, "Link: Exploiting the Web of data to generate test inputs", In _Proceedings of the 2014
International Symposium on Software Testing and Analysis (ISSTA)_
, 2014, pp. 373–384, downloadable at: https://www.researchgate.net/publication/265199482_Link_Exploiting_the_Web_of_Data_to_Generate_Test_Inputs

\[4\]: DBSplit, repository: https://gitlab.com/kandriann/dbsplit
