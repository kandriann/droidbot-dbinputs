from input_event import TouchEvent, ComplexEvent
from os.path import exists

from utilities.experiment.timings_calculator import write_KOs_progression, write_OKs_progression
from utilities.experiment.metrics_property import MetricsProperty


# to define dictionary of properties to check at runtime for each touch/complex event of interest for the application
# path to save output is also stored
def set_dictionary(application, path):  # KEY: ID-DESCRIPTION-TEXT of clicked button
    if application == "my-expenses":
        set_dictionary.ok_ko_dictionary = {
            # start new account. expected: start button is no more visible
            "org.totschnig.myexpenses:id/suw_navbar_done" + "-" + "None" + "-" + "GET STARTED": [
                MetricsProperty("org.totschnig.myexpenses:id/suw_navbar_done", "visible", False)],
            # add tag to account/transaction. expected: button in account activity has changed description
            "org.totschnig.myexpenses:id/CREATE_COMMAND" + "-" + "Confirm" + "-" + "None": [
                MetricsProperty("org.totschnig.myexpenses:id/CREATE_COMMAND", "content_description",
                                "Save the data and close the form.")],
            # add/edit account/transaction/template/payment-type/payee-debt
            # expected: button in home activity has changed description into "New" + smt
            # this is needed since the same key for diff buttons was set and check has to be made quite generic
            "org.totschnig.myexpenses:id/CREATE_COMMAND" "-" + "Save the data and close the form." + "-" + "None": [
                MetricsProperty("org.totschnig.myexpenses:id/CREATE_COMMAND", "content_description",
                                "New")],
            # add (sub)category/payee. # expected: alert an error message are not visible
            # alert alone is not enough since in case of duplicate values the alert is closed with error msg
            "android:id/button1" + "-" + "None" + "-" + "ADD": [
                MetricsProperty("org.totschnig.myexpenses:id/snackbar_text", "visible", False),
                MetricsProperty("org.totschnig.myexpenses:id/alertTitle", "visible", False)],
            # edit (sub)category/payee/tag.  expected: alert an error message are not visible
            # alert alone is not enough since in case of duplicate values the alert is closed with error msg
            "android:id/button1" + "-" + "None" + "-" + "SAVE": [
                MetricsProperty("org.totschnig.myexpenses:id/snackbar_text", "visible", False),
                MetricsProperty("org.totschnig.myexpenses:id/alertTitle", "visible", False)],
            # search transaction. expected: home is not empty
            "android:id/button1" + "-" + "None" + "-" + "SEARCH": [
                MetricsProperty("org.totschnig.myexpenses:id/empty", "visible", False)]
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "org.totschnig.myexpenses:id/suw_navbar_done" + "-" + "None" + "-" + "GET STARTED":
                "Start new account",
            "org.totschnig.myexpenses:id/CREATE_COMMAND" + "-" + "Confirm" + "-" + "None":
                "Add tag to account/transaction",
            "org.totschnig.myexpenses:id/CREATE_COMMAND" "-" + "Save the data and close the form." + "-" + "None":
                "Add/Edit account/transaction/template/payment-type/payee-debt",
            "android:id/button1" + "-" + "None" + "-" + "ADD":
                "Add category/payee",
            "android:id/button1" + "-" + "None" + "-" + "SAVE":
                "Edit category/payee/tag",
            "android:id/button1" + "-" + "None" + "-" + "SEARCH":
                "Search transaction"
        }
    elif application == "world-weather":
        set_dictionary.ok_ko_dictionary = {
            # search city. expected: alert error msg is not visible
            "com.haringeymobile.ukweather:id/search_go_btn" + "-" + "Submit query" + "-" + "None": [
                MetricsProperty("android:id/alertTitle", "visible", False)],
            # add city. expected: alert error msg is not visible
            "com.haringeymobile.ukweather:id/ac_search_button" + "-" + "None" + "-" + "None": [
                MetricsProperty("android:id/alertTitle", "visible", False)],
            # edit city. expected: alert error msg is not visible
            "android:id/button1" + "-" + "None" + "-" + "OK": [
                MetricsProperty("android:id/alertTitle", "visible", False)],
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "com.haringeymobile.ukweather:id/search_go_btn" + "-" + "Submit query" + "-" + "None":
                "Search city",
            "com.haringeymobile.ukweather:id/ac_search_button" + "-" + "None" + "-" + "None":
                "Add city",
            "android:id/button1" + "-" + "None" + "-" + "OK":
                "Edit city"
        }
    elif application == "mileage":
        set_dictionary.ok_ko_dictionary = {
            # edit fillup, fillup field, vehicle, vehicle type, service interval, or service interval templates
            # expected: save button is not visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Save changes": [
                MetricsProperty("com.evancharlton.mileage:id/save_btn", "visible", False)],
            # add fillup. expected: list of fillups is visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Save Fillup": [
                MetricsProperty("android:id/list", "visible", True)],
            # add fillup field. expected: add new fillup field button is not visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add new field": [
                MetricsProperty("com.evancharlton.mileage:id/save_btn", "visible", False)],
            # add vehicle. expected: add new vehicle button is not visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add new vehicle": [
                MetricsProperty("com.evancharlton.mileage:id/save_btn", "visible", False)],
            # add vehicle type. expected: add new vehicle type button is not visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add new vehicle type": [
                MetricsProperty("com.evancharlton.mileage:id/save_btn", "visible", False)],
            # add service interval. expected: add service interval button is not visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add service interval": [
                MetricsProperty("com.evancharlton.mileage:id/save_btn", "visible", False)],
            # add service interval template. expected: add service interval template button is not visible
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add service interval template": [
                MetricsProperty("com.evancharlton.mileage:id/save_btn", "visible", False)]
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Save changes":
                "Edit entry",
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Save Fillup":
                "Add fillup",
             "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add new field":
                "Add fillup field",
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add new vehicle":
                "Add vehicle",
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add new vehicle type":
                "Add vehicle type",
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add service interval":
                "Add service interval",
            "com.evancharlton.mileage:id/save_btn" + "-" + "None" + "-" + "Add service interval template":
                "Add service interval template"
        }
    elif application == "tricky-tripper":
        set_dictionary.ok_ko_dictionary = {
            # add trip/payment/create exchange rate. expected: add button is not visible
            "de.koelle.christian.trickytripper:id/option_save_create" + "-" + "Create" + "-" + "None": [
                MetricsProperty("de.koelle.christian.trickytripper:id/option_save_create", "visible", False)],
            # edit trip/payment/exchange rate/traveler. expected: save button is not visible
            "de.koelle.christian.trickytripper:id/option_save_edit" + "-" + "Save" + "-" + "None": [
                MetricsProperty("de.koelle.christian.trickytripper:id/option_save_edit", "visible", False)],
            # add traveler to trip. expected: just after the add, the button is disabled
            "de.koelle.christian.trickytripper:id/option_accept" + "-" + "Add" + "-" + "None": [
                MetricsProperty("de.koelle.christian.trickytripper:id/option_accept", "enabled", False)],
            # transfer money. expected: transfer button is not visible
            "de.koelle.christian.trickytripper:id/option_save_edit" + "-" + "Transfer" + "-" + "None": [
                MetricsProperty("de.koelle.christian.trickytripper:id/option_save_edit", "visible", False)],
            # exchange calculator. expected: exchange button is not visible
            "de.koelle.christian.trickytripper:id/option_accept" + "-" + "Use result" + "-" + "None": [
                MetricsProperty("de.koelle.christian.trickytripper:id/option_accept", "visible", False)]
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "de.koelle.christian.trickytripper:id/option_save_create" + "-" + "Create" + "-" + "None":
                "Add trip/payment/exchange rate",
            "de.koelle.christian.trickytripper:id/option_save_edit" + "-" + "Save" + "-" + "None":
                "Edit trip/payment/exchange rate/traveler",
            "de.koelle.christian.trickytripper:id/option_accept" + "-" + "Add" + "-" + "None":
                "Add traveler",
            "de.koelle.christian.trickytripper:id/option_save_edit" + "-" + "Transfer" + "-" + "None":
                "Transfer money",
            "de.koelle.christian.trickytripper:id/option_accept" + "-" + "Use result" + "-" + "None":
                "Calculate exchange"
        }
    elif application == "android-token":
        set_dictionary.ok_ko_dictionary = {
            # add token details. expected: add token details button and error msg are not visible
            "uk.co.bitethebullet.android.token:id/btnAddStep2" + "-" + "None" + "-" + "NEXT >": [
                MetricsProperty("uk.co.bitethebullet.android.token:id/btnAddStep2", "visible", False),
                MetricsProperty("android:id/message", "visible", False)],
            # add token key. expected: add token key button and error msg are not visible
            "uk.co.bitethebullet.android.token:id/tokenAddComplete" + "-" + "None" + "-" + "COMPLETE": [
                MetricsProperty("uk.co.bitethebullet.android.token:id/tokenAddComplete", "visible", False),
                MetricsProperty("android:id/message", "visible", False)],
            # change pin. expected: change pin button and error msg are not visible
            "uk.co.bitethebullet.android.token:id/pinChangeSubmit" + "-" + "None" + "-" + "SAVE": [
                MetricsProperty("uk.co.bitethebullet.android.token:id/pinChangeSubmit", "visible", False),
                MetricsProperty("android:id/message", "visible", False)],
            # remove pin. expected: remove pin button and error msg are not visible
            "uk.co.bitethebullet.android.token:id/pinRemoveSubmit" + "-" + "None" + "-" + "REMOVE": [
                MetricsProperty("uk.co.bitethebullet.android.token:id/pinRemoveSubmit", "visible", False),
                MetricsProperty("android:id/message", "visible", False)]
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "uk.co.bitethebullet.android.token:id/btnAddStep2" + "-" + "None" + "-" + "NEXT >":
                "Add token details",
            "uk.co.bitethebullet.android.token:id/tokenAddComplete" + "-" + "None" + "-" + "COMPLETE":
                "Add token key",
            "uk.co.bitethebullet.android.token:id/pinChangeSubmit" + "-" + "None" + "-" + "SAVE":
                "Change PIN",
            "uk.co.bitethebullet.android.token:id/pinRemoveSubmit" + "-" + "None" + "-" + "REMOVE":
                "Remove PIN"
        }
    elif application == "hibi":
        set_dictionary.ok_ko_dictionary = {
            # add/edit entry. expected: add entry button is not visible
            "com.marcdonald.hibi:id/btn_save" + "-" + "None" + "-" + "Save": [
                MetricsProperty("com.marcdonald.hibi:id/btn_save", "visible", False)],
            # add/edit tag. expected: add tag button is not visible
            "com.marcdonald.hibi:id/btn_save_tag" + "-" + "None" + "-" + "Save": [
                MetricsProperty("com.marcdonald.hibi:id/btn_save_tag", "visible", False)],
            # add/edit book. expected: add book button is not visible
            "com.marcdonald.hibi:id/btn_save_book" + "-" + "None" + "-" + "Save": [
                MetricsProperty("com.marcdonald.hibi:id/btn_save_book", "visible", False)],
            # add/edit word. expected: add word button is not visible
            "com.marcdonald.hibi:id/btn_save_new_word" + "-" + "None" + "-" + "Save": [
                MetricsProperty("com.marcdonald.hibi:id/btn_save_new_word", "visible", False)],
            # add/edit location. expected: add location button is not visible
            "com.marcdonald.hibi:id/btn_save_location" + "-" + "None" + "-" + "Save": [
                MetricsProperty("com.marcdonald.hibi:id/btn_save_location", "visible", False)],
            # search entry. expected: no results found is not visible
            "com.marcdonald.hibi:id/img_search_button" + "-" + "Search" + "-" + "None": [
                MetricsProperty("com.marcdonald.hibi:id/lin_search_no_results", "visible", False)],
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "com.marcdonald.hibi:id/btn_save" + "-" + "None" + "-" + "Save":
                "Add/Edit entry",
            "com.marcdonald.hibi:id/btn_save_tag" + "-" + "None" + "-" + "Save":
                "Add/Edit tag",
            "com.marcdonald.hibi:id/btn_save_book" + "-" + "None" + "-" + "Save":
                "Add/Edit book",
            "com.marcdonald.hibi:id/btn_save_new_word" + "-" + "None" + "-" + "Save":
                "Add/Edit word",
            "com.marcdonald.hibi:id/btn_save_location" + "-" + "None" + "-" + "Save":
                "Add/Edit location",
            "com.marcdonald.hibi:id/img_search_button" + "-" + "Search" + "-" + "None":
                "Search entry"
        }
    elif application == "diaguard":
        set_dictionary.ok_ko_dictionary = {
            # add tag. expected: tag field to fill while adding tags is not visible
            "android:id/button1" + "-" + "None" + "-" + "OK": [
                MetricsProperty("com.faltenreich.diaguard:id/input", "visible", False)],
            # add/edit entry/food. expected: input note field to fill while adding entries is not visible
            "com.faltenreich.diaguard:id/fab_container" + "-" + "New entry" + "-" + "None": [
                MetricsProperty("com.faltenreich.diaguard:id/note_input", "visible", False)],
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "android:id/button1" + "-" + "None" + "-" + "OK":
                "Add tag",
            "com.faltenreich.diaguard:id/fab_container" + "-" + "New entry" + "-" + "None":
                "Add/Edit entry/food"
        }

    elif application == "antennapod":
        set_dictionary.ok_ko_dictionary = {
            # add podcast. expected: Results are visible
            "de.danoeh.antennapod:id/searchButton" + "-" + "Search podcast…" + "-" + "None": [
                MetricsProperty("de.danoeh.antennapod:id/txtvTitle", "visible", True)],
            # select countries for/rename/edit info/add tag to podcast. expected: OK button no more visible
            "android:id/button1" + "-" + "None" + "-" + "OK": [
                MetricsProperty("android:id/button1", "visible", False)],
            # add podcast by RSS/podcast authentication/autoskip podcast.
            # expected: confirm button no more visible and error msg not present
            "android:id/button1" + "-" + "None" + "-" + "Confirm": [
                MetricsProperty("android:id/button1", "visible", False),
                MetricsProperty("android:id/message", "visible", False)],
            # set sleep timer. expected: set sleep timer msg not present
            "de.danoeh.antennapod:id/setSleeptimerButton" + "-" + "None" + "-" + "Set sleep timer": [
                MetricsProperty("de.danoeh.antennapod:id/setSleeptimerButton", "visible", False)]
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "de.danoeh.antennapod:id/searchButton" + "-" + "Search podcast…" + "-" + "None":
                "Add/Search podcast",
            "android:id/button1" + "-" + "None" + "-" + "OK":
                "Select countries for/Edit/Rename/Add tag to podcasts",
            "android:id/button1" + "-" + "None" + "-" + "Confirm":
                "Add podcast by RSS/Authentication/Autoskip",
            "de.danoeh.antennapod:id/setSleeptimerButton" + "-" + "None" + "-" + "Set sleep timer":
                "Set sleep timer",
        }

    elif application == "ankidroid":
        set_dictionary.ok_ko_dictionary = {
            # create deck/create tag/set various deck options. expected: alert msg not present
            "com.ichi2.anki:id/md_button_positive" + "-" + "None" + "-" + "OK": [
                MetricsProperty("com.ichi2.anki:id/md_text_title", "visible", False)],
            # create card/save card. expected: error msg not present
            "com.ichi2.anki:id/action_save" + "-" + "Save" + "-" + "None": [
                MetricsProperty("com.ichi2.anki:id/snackbar_text", "visible", False)],
            # set deck limit/add field to card/reposition field/set Anki dir/set max backups/set ahead limit/set timebox
            # set doubletap time. expected: alert msg not present
            "android:id/button1" + "-" + "None" + "-" + "OK": [
                MetricsProperty("android:id/button1", "visible", False)],
            # rename field. expected: alert msg not present
            "android:id/button1" + "-" + "None" + "-" + "Rename": [
                MetricsProperty("android:id/button1", "visible", False)],
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "com.ichi2.anki:id/md_button_positive" + "-" + "None" + "-" + "OK":
                "Create deck/Create tag/Various deck options",
            "com.ichi2.anki:id/action_save" + "-" + "Save" + "-" + "None":
                "Create card/Save card",
            "android:id/button1" + "-" + "None" + "-" + "OK":
                "Various settings",
            "android:id/button1" + "-" + "None" + "-" + "Rename":
                "Rename field",
        }

    elif application == "mantis":
        set_dictionary.ok_ko_dictionary = {
            # add issue. expected: add issue button no more present
            "se.nextsource.android.mantisdroidfree:id/issue_edit_menu_save_issue" + "-" + "Save" + "-" + "None": [
                MetricsProperty("se.nextsource.android.mantisdroidfree:id/issue_edit_menu_save_issue", "visible", False)],
            # search issue/add tag/day limits. expected: alert msg no more present, issue details present
            "android:id/button1" + "-" + "None" + "-" + "OK": [
                MetricsProperty("se.nextsource.android.mantisdroidfree:id/alertTitle", "visible", False),
                MetricsProperty("se.nextsource.android.mantisdroidfree:id/issue_details_project_name", "visible", True)],
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "se.nextsource.android.mantisdroidfree:id/issue_edit_menu_save_issue" + "-" + "Save" + "-" + "None":
                "Add issue",
            "android:id/button1" + "-" + "None" + "-" + "OK":
                "Search issue/Add tag/Set day limits"
        }


    elif application == "wordpress":
        set_dictionary.ok_ko_dictionary = {
            # internal login (under top-right label > Plus). expected: login help button is no more present
            "org.wordpress.android:id/bottom_button" + "-" + "None" + "-" + "Continue": [
                MetricsProperty("org.wordpress.android:id/login_site_address_help_button", "visible", False)],
            # search language (under Me > App Settings). expected: empty list msg not present
            "org.wordpress.android:id/text_input_start_icon" + "-" + "None" + "-" + "None": [
                MetricsProperty("org.wordpress.android:id/empty_view", "visible", False)],
            # add entry. expected: add button not present
            "android:id/button1" + "-" + "None" + "-" + "OK": [
                MetricsProperty("android:id/button1", "visible", False)],
            # add post category. expected: add button not present
            "org.wordpress.android:id/submit_button" + "-" + "None" + "-" + "ADD CATEGORY": [
                MetricsProperty("org.wordpress.android:id/submit_button", "visible", False)],
        }
        set_dictionary.readable_ok_ko_dictionary = {
            "org.wordpress.android:id/bottom_button" + "-" + "None" + "-" + "Continue":
                "Internal Login",
            "org.wordpress.android:id/text_input_start_icon" + "-" + "None" + "-" + "None":
                "Search Post Language",
            "android:id/button1" + "-" + "None" + "-" + "OK":
                "Add Entry",
            "org.wordpress.android:id/submit_button" + "-" + "None" + "-" + "ADD CATEGORY":
                "Add Category",
        }





    # set the output path where to save run results (OK-KO, visited activities, dbinputs matches)
    set_dictionary.output_path = path
    set_dictionary.application = application





# dictionary saving the properties (see set_dictionary()) that have to be checked after each touch/complex event
# key is the clicked button (resource + desc + text), value is the list of properties to check after the event
# each value is identified by a (list of) MetricsProperty as a triple <target element, property name, property value>
set_dictionary.ok_ko_dictionary = {}

# this is merely used for output
set_dictionary.readable_ok_ko_dictionary = {}

# dictionary saving OK counters for each complex event
set_dictionary.complex_events_ok_counter = {}
# dictionary saving KO counters for each complex event
set_dictionary.complex_events_ko_counter = {}
# dictionary saving OK counters for each touch event
set_dictionary.touch_events_ok_counter = {}
# dictionary saving KO counters for each touch event
set_dictionary.touch_events_ko_counter = {}

# output path to save OK and KO counters for each complex event of interest
set_dictionary.output_path = ""




def count_oks():
    all_complex_oks_values = list(set_dictionary.complex_events_ok_counter.values())
    all_touch_oks_values = list(set_dictionary.touch_events_ok_counter.values())
    total_oks = sum(i for i in all_complex_oks_values) + sum(i for i in all_touch_oks_values)
    # unique_oks = sum(i > 0 for i in all_complex_oks_values) + sum(i > 0 for i in all_touch_oks_values)
    # differently from all oks, unique oks must consider unique keys that are common in complex and touch events
    unique_oks = 0
    all_complex_oks_keys = list(set_dictionary.complex_events_ok_counter.keys())
    all_touch_oks_keys = list(set_dictionary.touch_events_ok_counter.keys())
    all_complex_oks_keys.extend(all_touch_oks_keys)
    all_unique_keys = set(all_complex_oks_keys)
    for k in all_unique_keys:
        if (k in set_dictionary.complex_events_ok_counter and set_dictionary.complex_events_ok_counter[k] > 0) or \
           (k in set_dictionary.touch_events_ok_counter and set_dictionary.touch_events_ok_counter[k] > 0):
            unique_oks = unique_oks + 1
    return total_oks, unique_oks

def count_kos():
    all_complex_kos = list(set_dictionary.complex_events_ko_counter.values())
    all_touch_kos = list(set_dictionary.touch_events_ko_counter.values())
    total_kos = sum(i for i in all_complex_kos) + sum(i for i in all_touch_kos)
    # unique_kos = sum(i > 0 for i in all_complex_kos) + sum(i > 0 for i in all_touch_kos)
    # differently from all kos, unique kos must consider unique keys that are common in complex and touch events
    unique_kos = 0
    all_complex_kos_keys = list(set_dictionary.complex_events_ko_counter.keys())
    all_touch_kos_keys = list(set_dictionary.touch_events_ko_counter.keys())
    all_complex_kos_keys.extend(all_touch_kos_keys)
    all_unique_keys = set(all_complex_kos_keys)
    for k in all_unique_keys:
        if (k in set_dictionary.complex_events_ko_counter and set_dictionary.complex_events_ko_counter[k] > 0) or \
           (k in set_dictionary.touch_events_ko_counter and set_dictionary.touch_events_ko_counter[k] > 0):
            unique_kos = unique_kos + 1
    return total_kos, unique_kos











def check_ok_ko_for_event(event_key, device, activity_before_event, dict_ok, dict_ko, num_event):
    # the key used for button is extended with the activity (in order to try discriminate same button keys)
    final_key = event_key + "#" + ExtrapolateId(activity_before_event)
    # the dictionary for ok-ko of buttons is initialized
    if final_key not in dict_ok:
        dict_ok[final_key] = 0
    if final_key not in dict_ko:
        dict_ko[final_key] = 0
    current_state = device.get_current_state()  # get the current state
    current_views = current_state.views  # get the current views (each view has form {prop-name: prop-value, ...})
    all_views_ids = list()
    for view in current_views:
        all_views_ids.append(view["resource_id"])
    properties_to_check = set_dictionary.ok_ko_dictionary[event_key]  # get properties to check
    num_checked_properties = 0
    num_to_check_properties = len(properties_to_check)
    for p in properties_to_check:  # iterate over current views-properties to check for OK-KO of event
        # if the check is about a view that has not to exist
        if p.get_property_name() == "visible" and p.get_property_value() is False:
            # if the view does not exist, the check is successful and goes on
            if p.get_property_target() not in all_views_ids:
                num_checked_properties += 1
                continue
            # else, KO and stop
            else:
                print("PROPERTIES CHECK FAILED BECAUSE THE EXPECTED MISSING VIEW IS STILL PRESENT AFTER CLICK")
                print("KO SAVED FOR " + event_key)
                dict_ko[final_key] += 1
                KOs = count_kos()
                write_KOs_progression(KOs[0], KOs[1], num_event)
                return None
        # if the check is about a property of a view that exists
        for view in current_views:
            # if the target view is found
            if view["resource_id"] == p.get_property_target():
                # if the view does not include expected property, KO and stop
                if not p.get_property_name() in view:
                    print("PROPERTIES CHECK FAILED BECAUSE OF MISSING PROPERTIES IN EXPECTED VIEWS AFTER CLICK")
                    print("KO SAVED FOR " + event_key)
                    dict_ko[final_key] += 1
                    KOs = count_kos()
                    write_KOs_progression(KOs[0], KOs[1], num_event)
                    return None
                # if the view violates expected property, KO and stop
                elif not view[p.get_property_name()] == p.get_property_value() and \
                        not p.get_property_value() in view[p.get_property_name()]:
                    print("PROPERTIES CHECK FAILED BECAUSE OF WRONG PROPERTIES IN EXPECTED VIEWS AFTER CLICK")
                    print("KO SAVED FOR " + event_key)
                    dict_ko[final_key] += 1
                    KOs = count_kos()
                    write_KOs_progression(KOs[0], KOs[1], num_event)
                    return None
                # if the view satisfies the desired property, the check is successful and goes on
                else:
                    num_checked_properties += 1
                    break
    if num_checked_properties == num_to_check_properties:  # if all checks are successful, OK
        print("PROPERTIES CHECK PASSED AFTER CLICK")
        print("OK SAVED FOR " + event_key)
        dict_ok[final_key] += 1
        OKs = count_oks()
        write_OKs_progression(OKs[0], OKs[1], num_event)
    else:  # if the current state does not include some of the expected views to check properties on, KO
        print("PROPERTIES CHECK FAILED BECAUSE OF MISSING EXPECTED VIEWS AFTER CLICK")
        print("KO SAVED FOR " + event_key)
        dict_ko[final_key] += 1
        KOs = count_kos()
        write_KOs_progression(KOs[0], KOs[1], num_event)


def check_ok_ko(event, device, activity_before_event, num_event):
    # if event is not a complex or touch event, stop
    if not (isinstance(event, ComplexEvent)) and not (isinstance(event, TouchEvent)):
        return None

    if isinstance(event, ComplexEvent):
        touch_event = event.events[-1]  # get touch event from list of events (always last one, see input_policy)
        event_key = str(touch_event.view["resource_id"]) + "-" + str(touch_event.view["content_description"]) + "-" + \
                    str(touch_event.view["text"])
        check_ok_ko_for_event(event_key, device, activity_before_event, set_dictionary.complex_events_ok_counter,
              set_dictionary.complex_events_ko_counter, num_event)
    else:
        if set_dictionary.application == "diaguard":
            return None
        touch_event = event
        # whereas complex events are build by default on "interesting" buttons hence a check is not needed
        # touch event needs to check if the button is one among those "interesting"
        event_key = str(touch_event.view["resource_id"]) + "-" + str(touch_event.view["content_description"]) + "-" + \
                    str(touch_event.view["text"])
        if not (event_key in set_dictionary.ok_ko_dictionary):
            return None;
        check_ok_ko_for_event(event_key, device, activity_before_event, set_dictionary.touch_events_ok_counter,
              set_dictionary.touch_events_ko_counter, num_event)




# update the visited activities
def update_visited_activities(activity):
    activity = ExtrapolateId(activity)
    if activity not in update_visited_activities.visited_activities:
        update_visited_activities.visited_activities[activity] = 1
    else:
        update_visited_activities.visited_activities[activity] += 1

# dictionary saving visited activities and # of times
update_visited_activities.visited_activities = {}

def all_activities(activities):
    all_activities.names = activities

# list saving all activities of current apk
all_activities.names = list()





def save_ok_ko_activities_file():
    # save OK-KO
    file_path = set_dictionary.output_path + "\\OKs-KOs.txt"
    file_exists = exists(file_path)
    if not file_exists:
        f = open(file_path, "x")
    else:
        f = open(file_path, "a")
    f.write("OK-KO COUNTERS FOR COMPLEX EVENTS\n")
    complex_events_keys = list()
    touch_events_keys = list()
    for k in set_dictionary.complex_events_ok_counter:
        k_split = k.rsplit("#")  # split between button (used for better output namings) and activity keys
        complex_events_keys.append(k_split[0])
        f.write(str(set_dictionary.readable_ok_ko_dictionary[k_split[0]]) + " (" + k_split[1] + ")" +
        " #OK " + str(set_dictionary.complex_events_ok_counter[k]) + " #KO " + str(set_dictionary.complex_events_ko_counter[k]) + "\n")
    for k in set_dictionary.ok_ko_dictionary.keys():
        if k not in complex_events_keys:
            f.write(str(set_dictionary.readable_ok_ko_dictionary[k]) + " #OK 0" + " #KO 0\n")  # OK-KO=0 if not visited
    f.write("============\nOK-KO COUNTERS FOR TOUCH EVENTS\n")
    for k in set_dictionary.touch_events_ok_counter:
        k_split = k.rsplit("#")  # split between button (used for better output namings) and activity keys
        touch_events_keys.append(k_split[0])
        f.write(str(set_dictionary.readable_ok_ko_dictionary[k_split[0]]) + " (" + k_split[1] + ")" +
        " #OK " + str(set_dictionary.touch_events_ok_counter[k]) + " #KO " + str(set_dictionary.touch_events_ko_counter[k]) + "\n")
    for k in set_dictionary.ok_ko_dictionary.keys():
        if k not in touch_events_keys:
            f.write(str(set_dictionary.readable_ok_ko_dictionary[k]) + " #OK 0" + " #KO 0\n")  # OK-KO=0 if not visited
    f.close()
    # save activities
    file_path = set_dictionary.output_path + "\\visited_activities.txt"
    file_exists = exists(file_path)
    if not file_exists:
        f = open(file_path, "x")
    else:
        f = open(file_path, "a")
    for n in all_activities.names:
        n = ExtrapolateId(n)
        if n in update_visited_activities.visited_activities:
            f.write("Activity " + n + " # visits " + str(update_visited_activities.visited_activities[n]) + "\n")
        else:
            f.write("Activity " + n + " # visits 0\n")
    f.close()

def ExtrapolateId(string):
    if string is None:
        return ""
    if "/" in string:
        s = string.rsplit("/")[-1]
        if "." in s:
            s = s.rsplit(".")[-1]
    elif "." in string:
        s = string.rsplit(".")[-1]
    else:
        s = string
    return s