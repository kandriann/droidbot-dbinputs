
class MetricsProperty:

    # e.g., property_target = "mypackage:id/homeMsg", property_name = "text", property_value = "Hello World"
    # e.g., property_target = "mypackage:id/homeButton", property_name = "visible", property_value = "true"
    def __init__(self, property_target, property_name, property_value):
        self.property_target = property_target
        self.property_name = property_name
        self.property_value = property_value

    # getter method
    def get_property_target(self):
        return self.property_target

    # setter method
    def set_property_target(self, property_target):
        self.property_target = property_target

    # getter method
    def get_property_name(self):
        return self.property_name

    # setter method
    def set_property_name(self, property_name):
        self.property_name = property_name

    # getter method
    def get_property_value(self):
        return self.property_value

    # setter method
    def set_property_value(self, property_value):
        self.property_value = property_value
