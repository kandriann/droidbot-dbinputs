import os
from datetime import *

from utilities.experiment import metrics_counter


def exp_counter(droidbot_start_time):
    exp_counter.start_time = droidbot_start_time
    exp_counter.file_path = metrics_counter.set_dictionary.output_path + "\\progression"
    isExist = os.path.exists(exp_counter.file_path)
    if not isExist:
        os.makedirs(exp_counter.file_path)


exp_counter.file_path = None

exp_counter.start_time = 0

exp_counter.dbinputs_cost = 0
exp_counter.link_cost = 0
exp_counter.dbsplit_cost = 0
exp_counter.other_experimental_cost = 0  # to sum up calls to metrics and file writings not needed by original Droidbot


def write_OKs_progression(total_OKs, unique_OKs, num_event):
    # print total and unique OKs encountered by timestamps
    spent_time = int((datetime.now() - exp_counter.start_time).total_seconds())
    new = False
    total_OKs_TS_progression_file = exp_counter.file_path + "\\totalOKs_timestamps.csv"
    if not os.path.isfile(total_OKs_TS_progression_file):
        new = True
    with open(total_OKs_TS_progression_file, 'a') as log:
        if new:
            log.write("Time(s);Tot OKs(#)\n")
        log.write(str(spent_time) + ";" + str(total_OKs) + "\n")
    new = False
    unique_OKs_TS_progression_file = exp_counter.file_path + "\\uniqueOKs_timestamps.csv"
    if not os.path.isfile(unique_OKs_TS_progression_file):
        new = True
    with open(unique_OKs_TS_progression_file, 'a') as log:
        if new:
            log.write("Time(s);Unique OKs(#)\n")
        log.write(str(spent_time) + ";" + str(unique_OKs) + "\n")
    # print total and unique OKs encountered by num events
    new = False
    total_OKs_events_progression_file = exp_counter.file_path + "\\totalOKs_events.csv"
    if not os.path.isfile(total_OKs_events_progression_file):
        new = True
    with open(total_OKs_events_progression_file, 'a') as log:
        if new:
            log.write("Event(#);Tot OKs(#)\n")
        log.write(str(num_event + 1) + ";" + str(total_OKs) + "\n")
    new = False
    unique_OKs_events_progression_file = exp_counter.file_path + "\\uniqueOKs_events.csv"
    if not os.path.isfile(unique_OKs_events_progression_file):
        new = True
    with open(unique_OKs_events_progression_file, 'a') as log:
        if new:
            log.write("Event(#);Unique OKs(#)\n")
        log.write(str(num_event + 1) + ";" + str(unique_OKs) + "\n")


def write_KOs_progression(total_KOs, unique_KOs, num_event):
    # print total and unique KOs encountered by timestamps
    spent_time = int((datetime.now() - exp_counter.start_time).total_seconds())
    new = False
    total_KOs_TS_progression_file = exp_counter.file_path + "\\totalKOs_timestamps.csv"
    if not os.path.isfile(total_KOs_TS_progression_file):
        new = True
    with open(total_KOs_TS_progression_file, 'a') as log:
        if new:
            log.write("Time(s);Tot KOs(#)\n")
        log.write(str(spent_time) + ";" + str(total_KOs) + "\n")
    new = False
    unique_KOs_TS_progression_file = exp_counter.file_path + "\\uniqueKOs_timestamps.csv"
    if not os.path.isfile(unique_KOs_TS_progression_file):
        new = True
    with open(unique_KOs_TS_progression_file, 'a') as log:
        if new:
            log.write("Time(s);Unique KOs(#)\n")
        log.write(str(spent_time) + ";" + str(unique_KOs) + "\n")
    # print total and unique KOs encountered by num events
    new = False
    total_KOs_events_progression_file = exp_counter.file_path + "\\totalKOs_events.csv"
    if not os.path.isfile(total_KOs_events_progression_file):
        new = True
    with open(total_KOs_events_progression_file, 'a') as log:
        if new:
            log.write("Event(#);Tot KOs(#)\n")
        log.write(str(num_event + 1) + ";" + str(total_KOs) + "\n")
    new = False
    unique_KOs_events_progression_file = exp_counter.file_path + "\\uniqueKOs_events.csv"
    if not os.path.isfile(unique_KOs_events_progression_file):
        new = True
    with open(unique_KOs_events_progression_file, 'a') as log:
        if new:
            log.write("Event(#);Unique KOs(#)\n")
        log.write(str(num_event + 1) + ";" + str(unique_KOs) + "\n")


def write_activity_coverage_progression(coverage, num_event):
    # print activity coverage by timestamps
    new = False
    activity_coverage_TS_progression_file = exp_counter.file_path + "\\activityCoverage_timestamps.csv"
    if not os.path.isfile(activity_coverage_TS_progression_file):
        new = True
    with open(activity_coverage_TS_progression_file, 'a') as log:
        spent_time = int((datetime.now() - exp_counter.start_time).total_seconds())
        if new:
            log.write("Time(s);Coverage(%)\n")
        log.write(str(spent_time) + ";" + str(int(coverage)) + "\n")
    # print activity coverage by num events
    new = False
    activity_coverage_events_progression_file = exp_counter.file_path + "\\activityCoverage_events.csv"
    if not os.path.isfile(activity_coverage_events_progression_file):
        new = True
    with open(activity_coverage_events_progression_file, 'a') as log:
        if new:
            log.write("Event(#);Coverage(%)\n")
        log.write(str(num_event + 1) + ";" + str(int(coverage)) + "\n")


def save_dbinputs_cost(cost):
    exp_counter.dbinputs_cost = exp_counter.dbinputs_cost + cost


def save_dbsplit_cost(cost):
    exp_counter.dbsplit_cost = exp_counter.dbsplit_cost + cost


def save_link_cost(cost):
    exp_counter.link_cost = exp_counter.link_cost + cost


def save_other_experimental_cost(cost):
    exp_counter.other_experimental_cost = exp_counter.other_experimental_cost + cost


def write_costs_to_file():
    additional_exp_costs = metrics_counter.set_dictionary.output_path + "\\additional_exp_cost.txt"
    with open(additional_exp_costs, 'a') as log:
        log.write(f"DBInputs Cost: {exp_counter.dbinputs_cost}\n")
        log.write(f"DBSplit Cost:  {exp_counter.dbsplit_cost}\n")
        log.write(f"Link Cost: {exp_counter.link_cost}\n")
        log.write(f"Others Cost: {exp_counter.other_experimental_cost}\n")
        log.write("==============\n")
        log.write(f"Total Additional Cost: "
                  f"{exp_counter.dbinputs_cost + exp_counter.dbsplit_cost + exp_counter.link_cost + exp_counter.other_experimental_cost}\n")
