import configparser
import subprocess
import ntpath
from pathlib import Path


def apply_dbsplit(device, app):
    # read from config file
    parser = configparser.ConfigParser()
    parser.read(device.config)
    dbsplit_jar_path = parser.get("config", "dbsplit_jar_path")
    use_device_db = parser.get("config", "use_device_db") == "true" or \
                    parser.get("config", "use_device_db") == "True"
    server = parser.get("config", "db_server")  # not needed by sqlite
    username = parser.get("config", "db_username")  # not needed by sqlite
    password = parser.get("config", "db_password")  # not needed by sqlite
    driver = parser.get("config", "db_driver")
    full_db_path = parser.get("config", "full_db")  # path to existing db in file system if device app db
    # is not used or path to pull the device app db to
    test_db_path = parser.get("config", "test_db")
    app_db_path = parser.get("config", "app_db")
    split_percent = int(parser.get("config", "split_percent"))
    split_type = parser.get("config", "split_type")
    overlapping_percent = int(parser.get("config", "overlapping_percent"))

    package_name = app.get_package_name()
    apply_dbsplit.package_name = package_name
    full_db_name = ntpath.basename(full_db_path)
    app_db_name = ntpath.basename(app_db_path)

    # open a adb shell (e.g. adb -s emulator-5554 shell)
    procId = subprocess.Popen('adb -s ' + device.serial + ' shell', stdin=subprocess.PIPE, encoding='utf8')

    # copy and rename the original DB to sdcard folder of device to allow pull and future restore
    procId.communicate('su \n ' +
                       ' cp data/data/' + package_name + '/databases/' + full_db_name + ' sdcard/' + full_db_name + '\n'
                       + ' mv sdcard/' + full_db_name + ' sdcard/TEMP' + full_db_name + '\n exit\n')

    # pull the original database from device if it has to be used
    if use_device_db:
        subprocess.Popen('adb pull sdcard/TEMP' + full_db_name + ' ' + full_db_path, stdin=subprocess.PIPE,
                         encoding='utf8')

    # apply DBSplit if half databases are not already present. at this point, full database must exist
    test_db_file = Path(test_db_path)
    app_db_file = Path(app_db_path)
    full_db_file = Path(full_db_path)
    if not full_db_file.is_file():
        raise Exception('No actual database was found. Check the path in config file.')
    if not (test_db_file.is_file() and app_db_file.is_file()):
        if split_type == "disjoint":
            subprocess.call('java -jar ' + dbsplit_jar_path + ' ' + server + ' ' + username + ' ' + password + ' '
                            + full_db_path + ' ' + test_db_path + ' ' + app_db_path + ' '
                            + str(split_percent) + ' ' + split_type + ' ' + driver, shell=True)
        else:  # in case of overlapping configuration
            subprocess.call('java -jar ' + dbsplit_jar_path + ' ' + server + ' ' + username + ' ' + password + ' '
                            + full_db_path + ' ' + test_db_path + ' ' + app_db_path + ' '
                            + str(split_percent) + ' ' + split_type + ' ' + str(overlapping_percent) + ' ' + driver,
                            shell=True)

    # push the app database half to device
    subprocess.Popen('adb push ' + app_db_path + ' sdcard/', stdin=subprocess.PIPE, encoding='utf8')

    # open a adb shell
    procId = subprocess.Popen('adb -s ' + device.serial + ' shell', stdin=subprocess.PIPE, encoding='utf8')

    # rename and copy the half app database to databases folder of device
    procId.communicate('su \n mv sdcard/' + app_db_name + ' sdcard/' + full_db_name + '\n' +
                       'cp -f sdcard/' + full_db_name + ' data/data/' + package_name + '/databases/' + full_db_name +
                       '\n exit\n')

apply_dbsplit.package_name = None  # to save the package name of the app under exploration


def restore_db(device):
    package_name = apply_dbsplit.package_name
    parser = configparser.ConfigParser()
    parser.read(device.config)
    full_db_path = parser.get("config", "full_db")
    full_db_name = ntpath.basename(full_db_path)

    # open a adb shell
    procId = subprocess.Popen('adb -s ' + device.serial + ' shell', stdin=subprocess.PIPE, encoding='utf8')

    # rename the original database as default, copy it to data/data/ folder of the app and remove temp occurrencies
    procId.communicate('su \n rm sdcard/' + full_db_name + '\n' +
                       ' mv sdcard/TEMP' + full_db_name + ' sdcard/' + full_db_name + '\n' +
                       ' cp -f sdcard/' + full_db_name + ' data/data/' + package_name + '/databases/' + full_db_name +
                       '\n' + ' rm sdcard/' + full_db_name + '\n exit\n')