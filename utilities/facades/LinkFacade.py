import configparser
import random
import subprocess
import json
from json import JSONDecodeError

from DBInputs.relations.fieldcolumncomparer.stringcomparer.EditDistanceStringComparer import EditDistanceStringComparer
from input_event import SetTextEvent
from DBInputs.structure.Field import Field
from DBInputs.structure.Label import Label
from DBInputs.structure.Activity import Activity
from utilities.experiment import metrics_counter

already_visited_textviews = {}  # key: activity + resource_id, value: original placeholder label
link_cache = {}  # key: resource_id, value: all values of all associated labels from Link
labels_cache = {}  # key: label, value: values from Link

class LinkFacade:

    def __init__(self, events, current_state, config):
        parser = configparser.ConfigParser()
        parser.read(config)
        self.link_path = parser.get("config", "link_jar_path")
        self.json_path = metrics_counter.set_dictionary.output_path + "\\output.json"
        self.views = dict()
        self.events = list()
        for event in events:
            self.views[event.view['temp_id']] = event.view
        self.activity_structure = None
        self.CreateActivityStructure(current_state)
        self.CallLink()


    def CreateActivityStructure(self, current_state):
        fields_list = list()
        package = self.ExtrapolateId(list(self.views.values())[0]["package"])
        activity = self.ExtrapolateId(current_state.foreground_activity)
        for key in self.views:
            labels_list = list()
            resource_id = self.ExtrapolateId(self.views[key]["resource_id"])
            # use resource_id as label (needed even if empty)
            labels_list.append(Label(resource_id))
            # get placeholder
            placeholder = self.views[key]["text"]
            # if the field has not been visited before
            if (activity + resource_id) not in already_visited_textviews:
                # if it's placeholder is non empty and non-numeric, save it as closest text
                if placeholder and not placeholder.isdigit():
                    already_visited_textviews[activity + resource_id] = placeholder
                # otherwise use the associated text computed in device_state.get_possible_inputs() as closest_text
                elif "associate_text_view" in self.views[key]:
                    already_visited_textviews[activity + resource_id] = (self.views[key]["associate_text_view"])[0][
                        "text"]
                # otherwise ignore the closest text
                else:
                    already_visited_textviews[activity + resource_id] = None
            closest_text = already_visited_textviews[activity + resource_id]
            if closest_text is not None:
                labels_list.append(Label(closest_text))
            fields_list.append(Field(labels_list))
        self.activity_structure = Activity(fields_list, package, activity)


    def ExtrapolateId(self, string):
        if string is None:
            return ""
        if "/" in string:
            resource_id = string.rsplit("/")[-1]
            if "." in resource_id:
                resource_id = resource_id.rsplit(".")[-1]
        elif "." in string:
            resource_id = string.rsplit(".")[-1]
        else:
            resource_id = string
        return resource_id


    def CallLink(self):
        '''
        l0 = Label("edt_content")
        l1 = Label("my_movie")
        l2 = Label("title334 saint_row4")
        f1 = Field([l0,l1,l2])
        l0 = Label("edt_search_bar")
        l1 = Label("movie mine")
        f2 = Field([l0, l1])
        l0 = Label("dffddfdf")
        f3 = Field([l0])
        p = Page([f1,f2,f3], self.page.package, self.page.activity)
        for field in p.GetFields():
        '''
        for field in self.activity_structure.GetFields():
            resource_id = field.labels[0].value
            if resource_id not in link_cache.keys():
                link_cache[resource_id] = self.GetValuesForField(field)


    # for each field, it collects all the values extracted by Link that are related to all tokens composing the labels
    def GetValuesForField(self, field):
        all_tokens = list()
        c = EditDistanceStringComparer()  # needed only to exploit its tokenizer method
        for label in field.labels:
            all_tokens.extend(c.GetLemmatizedTokens(label.value))
        all_values = list()
        for token in all_tokens:
            values = list()
            if token not in labels_cache.keys():
                self.ExecuteLink(token)  # this queries DBPedia via Link and populates JSON output file
                values = self.LoadValuesFromJSON()  # this retrieves values from JSON output file
                labels_cache[token] = values
            if token in labels_cache.keys() and labels_cache[token]:
                values = labels_cache[token]
            all_values.extend(values)
        return all_values


    def ExecuteLink(self, label):
        subprocess.call(['java', '-jar', self.link_path, '--labels', label, '--output', self.json_path,
                         '--endpoint', 'https://dbpedia.org/sparql/'])


    # for each label, it saves the list of values as retrieved by Link
    # the output data is extracted and overwritten at each Link call (i.e. for each label)
    def LoadValuesFromJSON(self):
        values = list()
        try:
            with open(self.json_path) as json_file:
                data = json.load(json_file)
            for prop in data.keys():
                values.extend(data[prop])
        except JSONDecodeError as e:
            print(e)
        return values


    # this method is called after Link data extraction, to configure all settext events using such data
    def GetSetTextEvents(self):
        for key in self.views:
            view_resource_id = self.ExtrapolateId(self.views[key]["resource_id"])
            if view_resource_id in link_cache.keys() and link_cache[view_resource_id]:
                values = link_cache[view_resource_id]
                text = random.choice(values)  # chose randomly among the retrieved values
            else:
                text = "Data not found"
            self.events.append(SetTextEvent(view=self.views[key], text=text))
        return self.events


